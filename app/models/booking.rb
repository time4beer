class Booking < ActiveRecord::Base
  validates_presence_of :contact, :start, :end
end
