set :application, "time4beer"
set :repository,  "http://fuzzy.homedns.org/git/time4beer.git"

# If you aren't deploying to /u/apps/#{application} on the target
# servers (which is the default), you can specify the actual location
# via the :deploy_to variable:
set :deploy_to, "/www-data/ruby/#{application}"

# If you aren't using Subversion to manage your source code, specify
# your SCM below:
set :scm, :git

role :app, "fuzzy.homedns.org"
role :web, "fuzzy.homedns.org"
role :db,  "fuzzy.homedns.org", :primary => true

set :use_sudo, false
set :scm_verbose, true

after "deploy:update_code", "db:symlink" 

namespace :db do
  desc "Make symlink for database yaml" 
  task :symlink do
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml" 
  end
end
