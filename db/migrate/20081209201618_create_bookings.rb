class CreateBookings < ActiveRecord::Migration
  def self.up
    create_table :bookings do |t|
      t.string :contact
      t.time :start
      t.time :end

      t.timestamps
    end
  end

  def self.down
    drop_table :bookings
  end
end
