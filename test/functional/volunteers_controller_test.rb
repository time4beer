require 'test_helper'

class VolunteersControllerTest < ActionController::TestCase
  def test_should_get_index
    get :index
    assert_response :success
    assert_not_nil assigns(:volunteers)
  end

  def test_should_get_new
    get :new
    assert_response :success
  end

  def test_should_create_volunteer
    assert_difference('Volunteer.count') do
      post :create, :volunteer => { }
    end

    assert_redirected_to volunteer_path(assigns(:volunteer))
  end

  def test_should_show_volunteer
    get :show, :id => volunteers(:one).id
    assert_response :success
  end

  def test_should_get_edit
    get :edit, :id => volunteers(:one).id
    assert_response :success
  end

  def test_should_update_volunteer
    put :update, :id => volunteers(:one).id, :volunteer => { }
    assert_redirected_to volunteer_path(assigns(:volunteer))
  end

  def test_should_destroy_volunteer
    assert_difference('Volunteer.count', -1) do
      delete :destroy, :id => volunteers(:one).id
    end

    assert_redirected_to volunteers_path
  end
end
